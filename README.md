openQA tests for the Fedora distribution
========================================

The Fedora openQA tests have moved to [Pagure](https://pagure.io/fedora-qa/os-autoinst-distri-fedora).
